### **Сервис модерации комментариев**

Является частью децентрализованного [**приложения**](https://github.com/rtemka/agg)

Тестовые запрещенные слова: **`"qwerty", "йцукен", "zxvbnm"`**

#### Эндпоинт

```bash
# запрос
curl -X POST -d '{"news_id":1, "text":"test comment example", "posted_at":1661262350, "author_id":1}' http://[host:port]/comments
# ответ 
# {"response": "allowed"} ... 200 OK
```

```bash
# запрос
curl -X POST -d '{"news_id":1, "text":"test comment example: zxvbnm", "posted_at":1661262350, "author_id":2}' http://[host:port]/comments
# ответ 
# {"response": "banned"} ... 400 Bad Request
```